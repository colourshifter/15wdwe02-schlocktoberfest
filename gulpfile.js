// Requirements
var gulp = require('gulp');
var gutil = require('gulp-util');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');

var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

var sftp = require('gulp-sftp');
var config = require('./gulp-config.json');

// Options

var sassOptions = {
  style: 'nested',
  errLogToConsole: true
};

var autoprefixerOptions = {
  browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};

var sassInput = './public_html/scss/*.scss';
var sassOutput = './public_html/css';

var scriptsInput = './public_html/scripts/*.js';
var scriptsOutput = './public_html/js/';
var scriptsConcat = 'app.min.js';

// Tasks

gulp.task('default', ['sass', 'scripts']);

gulp.task('sass', function () {
  return gulp
    .src(sassInput)
    .pipe(plumber({
        errorHandler: reportError
    }))
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptions))
    .pipe(sourcemaps.write())
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(gulp.dest(sassOutput))
    .on('error', reportError);
});

gulp.task('scripts', function () {
  return gulp
    .src(scriptsInput)
    .pipe(plumber({
      errorHandler: reportError
    }))
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(concat(scriptsConcat))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(scriptsOutput)
  );
});

gulp.task('watch', function() {
  return gulp
    // Watch the input folder for change,
    // and run `sass` task when something happens
    .watch([sassInput, scriptsInput], ['sass', 'scripts'])
    // When there is a change, log a message in the console
    .on('change', function(event) {
      console.log("");
      gutil.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});


gulp.task('deploy', ['deploy-site', 'deploy-config']);

gulp.task('deploy-site', function () {
  return gulp.src('public_html/**')
    .pipe(sftp({
        host: config.production.host,
        user: config.production.user,
        pass: config.production.pass,
        remotePath: config.production.webroot
    })
  );
});

gulp.task('deploy-config', function () {
  return gulp.src('schlocktoberfest-config.inc.php')
    .pipe(sftp({
      host: config.production.host,
      user: config.production.user,
      pass: config.production.pass,
      remotePath: config.production.webroot + '../'
    })
  );
});


// Error reporter

var reportError = function (error) {
    var lineNumber = (error.lineNumber) ? 'LINE ' + error.lineNumber + ' -- ' : '';

    var errorMessage = (error.message) ? error.message : "";

    notify({
        title: 'Task Failed [' + error.plugin + ']',
        message: errorMessage + "\n" + lineNumber + 'See console.',
        sound: 'Sosumi' // See: https://github.com/mikaelbr/node-notifier#all-notification-options-with-their-defaults
    }).write(error);

    gutil.beep(); // Beep 'sosumi' again

    // Inspect the error object
    //console.log(error);

    // Easy error reporting
    //console.log(error.toString());

    // Pretty error reporting
    var report = '';
    var chalk = gutil.colors.white.bgRed;

    report += chalk('TASK:') + ' [' + error.plugin + ']\n';
    report += chalk('PROB:') + ' ' + error.message + '\n';
    if (error.lineNumber) { report += chalk('LINE:') + ' ' + error.lineNumber + '\n'; }
    if (error.fileName)   { report += chalk('FILE:') + ' ' + error.fileName + '\n'; }
    console.error(report);

    // Prevent the 'watch' task from stopping
    this.emit('end');
};
